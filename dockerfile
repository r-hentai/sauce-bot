FROM python:3.10-slim as build

RUN pip install pdm
COPY ./pyproject.toml ./pdm.lock ./
RUN pdm export --prod -f requirements -o requirements.txt

FROM python:3.10-slim
ENV PYTHONPATH=$PYTHONPATH:/app/src \
    PYTHONBUFFERED=0 \
    PATH=$PATH:/home/app/.local/bin
RUN apt-get update && apt-get install -y curl

RUN adduser -u 1000 app
USER app

WORKDIR /app

COPY --from=build ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./src ./src
ENTRYPOINT ["python", "src/main.py"]
