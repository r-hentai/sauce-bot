import dataclasses

from asyncpraw.reddit import Submission


@dataclasses.dataclass
class SubmissionMessage:
    image_url: str
    submission: Submission
