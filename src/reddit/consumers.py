import asyncpraw
from anyio.streams.memory import MemoryObjectReceiveStream

from saucenao import SauceNaoClient

from .messages import SubmissionMessage


async def saucenao_consumer(
    nao_client: SauceNaoClient,
    reddit_client: asyncpraw.Reddit,
    receive: MemoryObjectReceiveStream[SubmissionMessage],
) -> None:
    async for message in receive:
        print(message)
