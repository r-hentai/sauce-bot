from typing import Protocol

from asyncpraw.reddit import Submission


class SaveSubmission(Protocol):
    async def save(self, submission: Submission) -> None:
        ...

    async def is_saved(self, submission: Submission) -> bool:
        ...


class LocalSaveSubmission:
    def __init__(self) -> None:
        self.submissions: set[str] = set()

    async def save(self, submission: Submission) -> None:
        self.submissions.add(submission.id)

    async def is_saved(self, submission: Submission) -> bool:
        return submission.id in self.submissions
