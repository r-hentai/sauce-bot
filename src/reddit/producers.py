import os.path
from urllib.parse import urlsplit

import asyncpraw
from anyio.streams.memory import MemoryObjectSendStream
from asyncpraw.models.reddit.subreddit import SubredditStream

from .messages import SubmissionMessage
from .submission_save import SaveSubmission


async def reddit_producer(
    send: MemoryObjectSendStream[SubmissionMessage],
    client: asyncpraw.Reddit,
    save_submissions: SaveSubmission,
) -> None:
    allowed_extensions = [".jpg", ".png", ".jpeg"]
    async with send:
        subreddit = await client.subreddit("hentai")
        async for submission in SubredditStream(subreddit).submissions():
            if await save_submissions.is_saved(submission):
                continue
            url = urlsplit(submission.url)
            _, ext = os.path.splitext(url.path)
            if ext not in allowed_extensions:
                continue

            await send.send(
                SubmissionMessage(
                    submission=submission,
                    image_url=submission.url,
                )
            )

            await save_submissions.save(submission)
