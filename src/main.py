import anyio
import asyncpraw
import dotenv
from anyio import create_memory_object_stream, create_task_group

from reddit.consumers import saucenao_consumer
from reddit.producers import reddit_producer
from reddit.submission_save import LocalSaveSubmission
from saucenao import SauceNaoClient
from settings import RedditSettings, SauceNaoSettings


async def main() -> None:
    dotenv.load_dotenv(".env")

    nao_client = SauceNaoClient(settings=SauceNaoSettings())
    reddit_settings = RedditSettings()
    reddit_client = asyncpraw.Reddit(
        client_id=reddit_settings.client_id,
        client_secret=reddit_settings.secret,
        user_agent=reddit_settings.user_agent,
    )

    async with reddit_client, nao_client:
        send, receive = create_memory_object_stream()
        async with create_task_group() as tg:
            tg.start_soon(reddit_producer, send, reddit_client, LocalSaveSubmission())
            tg.start_soon(saucenao_consumer, nao_client, reddit_client, receive)


if __name__ == "__main__":
    anyio.run(main)
