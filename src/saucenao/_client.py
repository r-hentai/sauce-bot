from types import TracebackType
from typing import Type

from httpx import AsyncClient

from saucenao.models import NaoResponse
from settings import SauceNaoSettings


class SauceNaoClient:
    def __init__(
        self,
        settings: SauceNaoSettings,
        client: AsyncClient | None = None,
    ) -> None:
        self._client = client or AsyncClient(
            base_url=settings.base_url,
        )
        self._settings = settings

    async def __aenter__(self) -> "SauceNaoClient":
        return self

    async def __aexit__(
        self,
        exc_type: Type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: TracebackType | None,
    ) -> None:
        await self._client.__aexit__(exc_type, exc_value, traceback)

    async def search(self, image_url: str) -> NaoResponse:
        response = await self._client.post(
            "/search.php",
            params={
                "url": image_url,
                "output_type": 2,
                "api_key": self._settings.api_key,
                "minsim": self._settings.min_similarity,
                "numres": self._settings.results,
                "db": 999,
            },
        )
        return NaoResponse.parse_obj(response.json())
