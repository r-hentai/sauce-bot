from pydantic import BaseModel


class NaoHeader(BaseModel):
    user_id: str
    account_type: str
    short_limit: int
    short_remaining: int
    long_limit: int
    long_remaining: int
    status: int


class NaoResult(BaseModel):
    class Header(BaseModel):
        similarity: float
        index_id: int
        index_name: str

    class Data(BaseModel):
        ext_urls: list[str]
        creator: str
        material: str
        characters: str
        source: str

    header: Header
    data: Data


class NaoResponse(BaseModel):
    header: NaoHeader
    results: list[NaoResult]
