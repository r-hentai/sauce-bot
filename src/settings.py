from pydantic import BaseSettings


class RedditSettings(BaseSettings):
    class Config:
        env_prefix = "reddit_"

    secret: str
    client_id: str
    user_agent: str


class SauceNaoSettings(BaseSettings):
    class Config:
        env_prefix = "saucenao_"

    api_key: str
    base_url: str = "https://saucenao.com"
    results: int = 1
    min_similarity: int = 70
